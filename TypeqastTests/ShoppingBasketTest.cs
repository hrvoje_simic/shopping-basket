﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Typeqast;

namespace TypeqastTests
{
    [TestClass]
    public class ShoppingBasketTest
    {
        private ShoppingBasket _basket;
        private Product butter = new Product() { Id = Constants.Products.Butter, MeasureUnit = "Piece", Name = "Butter", Price = 0.8m };
        private Product milk = new Product() { Id = Constants.Products.Milk, MeasureUnit = "Liter", Name = "Milk", Price = 1.15m };
        private Product bread = new Product() { Id = Constants.Products.Bread, MeasureUnit = "Piece", Name = "Bread", Price = 1m };

        [TestInitialize]
        public void Setup()
        {
            _basket = new ShoppingBasket();
        }

        [TestMethod]
        public void ZeroWhenEmpty()
        {
            Assert.AreEqual(0, _basket.TotalAmount());
        }

        [TestMethod]
        public void OneOfEachItem()
        {
            _basket.Add(new OrderProduct() { Product = butter, Quantity = 1 });
            _basket.Add(new OrderProduct() { Product = milk, Quantity = 1 });
            _basket.Add(new OrderProduct() { Product = bread, Quantity = 1 });
            Assert.AreEqual(2.95m, _basket.TotalAmount());
        }

        [TestMethod]
        public void TwoButtersTwoBreads()
        {
            _basket.Add(new OrderProduct() { Product = butter, Quantity = 2 });
            _basket.Add(new OrderProduct() { Product = bread, Quantity = 2 });
            Assert.AreEqual(3.1m, _basket.TotalAmount());
        }

        [TestMethod]
        public void FourMilks()
        {
            _basket.Add(new OrderProduct() { Product = milk, Quantity = 4 });
            Assert.AreEqual(3.45m, _basket.TotalAmount());
        }
        [TestMethod]
        public void TwoButterOneBreadEightMilks()
        {
            _basket.Add(new OrderProduct() { Product = butter, Quantity = 2 });
            _basket.Add(new OrderProduct() { Product = milk, Quantity = 8 });
            _basket.Add(new OrderProduct() { Product = bread, Quantity = 1 });
            Assert.AreEqual(9m, _basket.TotalAmount());
        }
    }
}
