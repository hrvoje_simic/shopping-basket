﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Typeqast.DiscountRules;

namespace Typeqast
{
    public class DiscountCalculator : IDiscountCalculator
    {
        private readonly List<IDiscountRule> _discountRules;

        /// <summary>
        /// In this way we can easily add additional rules followiung open/close principle without messing with the current code
        /// </summary>
        public DiscountCalculator()
        {
            _discountRules = new List<IDiscountRule>();
            _discountRules.Add(new ButterBreadRule());
            _discountRules.Add(new MilkRule());
        }

        /// <summary>
        /// Calculates total discount for all available rules
        /// </summary>
        /// <param name="basket"></param>
        /// <returns></returns>
        public decimal CalculateDiscount(IEnumerable<OrderProduct> basket)
        {
            if (basket == null || basket.Count() == 0)
                return 0m;

            decimal totalDiscount = 0m;
            
            foreach (var rule in _discountRules)
            {
                totalDiscount += rule.CalculateDiscount(basket);
            }

            return totalDiscount;
        }

    }
}
