﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Typeqast
{
    /// <summary>
    /// Defines one line in shopping basket
    /// </summary>
    public class OrderProduct
    {
        public Product Product { get; set; }
        public double Quantity { get; set; }
    }
}
