﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Typeqast
{
    /// <summary>
    /// Defines product 
    /// </summary>
    public class Product
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string MeasureUnit { get; set; }
        public decimal Price { get; set; }
    }
}
