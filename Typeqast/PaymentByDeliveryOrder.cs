﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Typeqast.Services;

namespace Typeqast
{
    public class PaymentByDeliveryOrder : Order
    {
        private readonly INotificationService _notificationService;
        private readonly DeliveryDetails _deliveryDetails;
        private readonly IReservationService _reservationService;

        public PaymentByDeliveryOrder(ShoppingBasket basket, DeliveryDetails deliveryDetails)
            : base(basket)
        {
            _deliveryDetails = deliveryDetails;
            _reservationService = new ReservationService();
            _notificationService = new MailNotificationService();
        }

        public override void Checkout()
        {
            _reservationService.ReserveInventory(_basket.Products);

            _notificationService.NotifyCustomerOrderCreated(_basket);

            base.Checkout();
        }
    }
}
