﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Typeqast
{
    public class Constants
    {
        public class Products
        {
            public const int Butter = 1;
            public const int Bread = 2;
            public const int Milk = 3;
        }
    }
}
