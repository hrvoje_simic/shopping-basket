﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Typeqast
{
    public abstract class Order
    {
        protected readonly ShoppingBasket _basket;

        protected Order(ShoppingBasket basket)
        {
            _basket = basket;
        }

        public virtual void Checkout()
        {

        }
    }
}
