﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Typeqast.Services;

namespace Typeqast
{
    public class CreditCardOrder : Order
    {
        private readonly INotificationService _notificationService;
        private readonly PaymentDetails _paymentDetails;
        private readonly IPaymentProcessor _paymentProcessor;
        private readonly IReservationService _reservationService;

        public CreditCardOrder(ShoppingBasket basket, PaymentDetails paymentDetails)
            : base(basket)
        {
            _paymentDetails = paymentDetails;
            _paymentProcessor = new PaymentProcessor();
            _reservationService = new ReservationService();
            _notificationService = new MailNotificationService();
        }

        public override void Checkout()
        {
            _paymentProcessor.ProcessCreditCard(_paymentDetails, _basket.TotalAmount());

            _reservationService.ReserveInventory(_basket.Products);

            _notificationService.NotifyCustomerOrderCreated(_basket);

            base.Checkout();
        }
    }
}
