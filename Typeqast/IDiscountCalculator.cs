﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Typeqast
{
    public interface IDiscountCalculator
    {
        decimal CalculateDiscount(IEnumerable<OrderProduct> basket);
    }
}
