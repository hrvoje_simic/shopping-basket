﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Typeqast
{
    public class ShoppingBasket
    {
        private readonly List<OrderProduct> _products;
        private readonly IDiscountCalculator _discountCalculator;

        /// <summary>
        /// Simple IOC
        /// </summary>
        public ShoppingBasket() : this(new DiscountCalculator())
        {
        }

        public ShoppingBasket(IDiscountCalculator discountCalculator)
        {
            _discountCalculator = discountCalculator;
            _products = new List<OrderProduct>();
        }

        /// <summary>
        /// Contains all products currently in the basket
        /// </summary>
        public IEnumerable<OrderProduct> Products
        {
            get { return _products; }
        }

        /// <summary>
        /// Adds a product to basket. If products already exists in basket it will increase quantity of the product
        /// </summary>
        /// <param name="orderProduct"></param>
        public void Add(OrderProduct orderProduct)
        {
            if (_products.Any(p => p.Product.Id == orderProduct.Product.Id))
                _products.Where(p => p.Product.Id == orderProduct.Product.Id).FirstOrDefault().Quantity += orderProduct.Quantity;
            else
                _products.Add(orderProduct);
        }


        /// <summary>
        /// Removes product with given id form the basket.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>It will return true if product is removed</returns>
        public bool Remove(int productId)
        {
            var product = _products.FirstOrDefault(p => p.Product.Id == productId);
            if (product != null)
                return _products.Remove(product);
            else
                return false;
        }

        /// <summary>
        /// Calculates total amount for all products in shopping basket. If there are applicable discounts they will be deducted from total price
        /// </summary>
        /// <returns>Total price of items in shopping basket including discounts</returns>
        public decimal TotalAmount()
        {
            decimal total = 0m;
            decimal discount = 0m;
            foreach (OrderProduct orderProduct in Products)
            {
                Debug.WriteLine($"Product: {orderProduct.Product.Name}, Price per unit: {orderProduct.Product.Price}, Quantity: {orderProduct.Quantity}");

                total += (decimal)orderProduct.Quantity * orderProduct.Product.Price;
            }

            Debug.WriteLine($"Total without discounts: {total}");

            discount = _discountCalculator.CalculateDiscount(_products);

            Debug.WriteLine($"Discounts: {discount}");
            Debug.WriteLine($"Total: {total - discount}");

            return total - discount;
        }
    }
}
