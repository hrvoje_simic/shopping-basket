﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Typeqast.DiscountRules
{
    public class MilkRule : IDiscountRule
    {
        const int _numMilksForDiscount = 4;

        /// <summary>
        /// Calculates discount for Milk rule gor current shopping basket
        /// </summary>
        /// <param name="basket"></param>
        /// <returns></returns>
        public decimal CalculateDiscount(IEnumerable<OrderProduct> basket)
        {
            decimal discount = 0m;

            var milk = basket.Where(p => p.Product.Id == Constants.Products.Milk).FirstOrDefault();

            //If there is no milk in basket or quantity is less than minimum there is no discount
            if (milk == null || milk.Quantity < _numMilksForDiscount)
                return discount;

            int numDiscountSets = (int)milk.Quantity / _numMilksForDiscount;

            discount = numDiscountSets * milk.Product.Price;

            return discount;
        }

        /// <summary>
        /// Checks can Milk discount be applyed to current shopping basket
        /// </summary>
        /// <param name="basket"></param>
        /// <returns></returns>
        public bool ShouldBeApplyed(IEnumerable<OrderProduct> basket)
        {
            var milk = basket.Where(p => p.Product.Id == Constants.Products.Milk).FirstOrDefault();

            if (milk == null || milk.Quantity < _numMilksForDiscount)
                return false;

            return true;
        }
    }
}
