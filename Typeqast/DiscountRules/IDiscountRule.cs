﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Typeqast
{
    public interface IDiscountRule
    {
        bool ShouldBeApplyed(IEnumerable<OrderProduct> basket);
        decimal CalculateDiscount(IEnumerable<OrderProduct> basket);
    }
}
