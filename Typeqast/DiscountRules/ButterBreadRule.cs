﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Typeqast.DiscountRules
{
    public class ButterBreadRule : IDiscountRule
    {
        const int _numButtersForDiscount = 2;

        /// <summary>
        /// Calculates discount for ButterBread rule gor current shopping basket
        /// </summary>
        /// <param name="basket"></param>
        /// <returns></returns>
        public decimal CalculateDiscount(IEnumerable<OrderProduct> basket)
        {
            decimal discount = 0m;

            var butter = basket.Where(p => p.Product.Id == Constants.Products.Butter).FirstOrDefault();

            //If there is no butter in basket or quantity is less than minimum there is no discount
            if (butter == null || butter.Quantity < _numButtersForDiscount)
                return 0m;

            //Checks how many time can discount be applied
            int numDiscountSets = (int)butter.Quantity / _numButtersForDiscount;

            var bread = basket.Where(p => p.Product.Id == Constants.Products.Bread).FirstOrDefault();

            //If there is no bread there is no product on which we can apply discount
            if (bread == null || bread.Quantity == 0)
                return 0m;

            //Calculates total discount
            if (bread.Quantity <= numDiscountSets)
                discount = (decimal)bread.Quantity * bread.Product.Price * 0.5m;
            else
                discount = numDiscountSets * bread.Product.Price * 0.5m;

            return discount;
        }
        

        /// <summary>
        /// Checks can ButterBread discount be applyed to current shopping basket
        /// </summary>
        /// <param name="basket"></param>
        /// <returns></returns>
        public bool ShouldBeApplyed(IEnumerable<OrderProduct> basket)
        {
            var butter = basket.Where(p => p.Product.Id == Constants.Products.Butter).FirstOrDefault();

            if (butter == null || butter.Quantity < _numButtersForDiscount)
                return false;

            var bread = basket.Where(p => p.Product.Id == Constants.Products.Bread).FirstOrDefault();

            if (bread == null || bread.Quantity == 0)
                return false;

            return true;
        }
    }
}
